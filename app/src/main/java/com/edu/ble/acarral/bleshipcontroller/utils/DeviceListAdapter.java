package com.edu.ble.acarral.bleshipcontroller.utils;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edu.ble.acarral.bleshipcontroller.R;

import java.util.ArrayList;

/**
 * Created by acarral on 11/26/16.
 */

public class DeviceListAdapter extends BaseAdapter{
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<BluetoothDevice> mDataSource;

    public DeviceListAdapter(Context mContext, ArrayList<BluetoothDevice> mDataSource) {
        this.mContext = mContext;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mDataSource = mDataSource;
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int i) {
        return mDataSource.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void addDevice(BluetoothDevice device) {
        this.mDataSource.add(device);
    }

    public void filter(BluetoothDevice device) {
        if(!exist(device.getAddress())) {
            addDevice(device);
        }
    }

    private boolean exist(String macAddress) {
        for(BluetoothDevice device : mDataSource) {
            if(device.getAddress().equals(macAddress)) {
                return true;
            }
        }
        return false;
    }

    public void clear(){
        mDataSource.clear();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = mInflater.inflate(R.layout.list_item_device, viewGroup, false);

        TextView titleTextView = (TextView) rowView.findViewById(R.id.device_list_title);

        TextView subtitleTextView = (TextView) rowView.findViewById(R.id.device_list_subtitle);

        BluetoothDevice device = (BluetoothDevice) getItem(position);
        titleTextView.setText(null == device.getName() || device.getName().isEmpty()? "Unnamed" : device.getName());
        subtitleTextView.setText(device.getAddress());

        return rowView;
    }
}
