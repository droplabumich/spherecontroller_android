package com.edu.ble.acarral.bleshipcontroller;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothGattDescriptor;


import android.content.res.Configuration;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.graphics.PorterDuff;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.CharacterCodingException;
import java.util.Queue;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.lang.Object;

/**
 * Created by acarral on 11/26/16.
 */

public class DeviceActivity extends AppCompatActivity {

    private static final UUID Battery_Service_UUID = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
    //private static final UUID Device_Information_Service_UUID = UUID.fromString("0000180A-0000-1000-8000-00805f9b34fb");

    private static final UUID SphereInfo_Service_UUID = UUID.fromString("51311102-030e-485f-b122-f8f381aa84ed");
    private static final UUID PowerRails_Characteristic_UUID = UUID.fromString("612c867a-07fa-40d3-8e21-afcd96e2109a");
    private static final UUID SphereStatus_Characteristic_UUID = UUID.fromString("485f4145-52b9-4644-af1f-7a6b9322490f");
    private static final UUID SphereTest_Characteristic_UUID = UUID.fromString("0757a65b-21fa-b422-5972-6c9283c71ca5");

    private static final UUID Battery_Level_UUID = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");


    // Queues for characteristic read (synchronous)
    private Queue<BluetoothGattCharacteristic> readQueue;
    private Queue<Pair> writeQueue;

    private BluetoothDevice device;
    private BluetoothGatt mGatt;

    private TextView address;
    private TextView batteryLevel;
    private TextView humidityLevel;
    private TextView temperatureLevel;
    private TextView pressureLevel;
    private TextView statusLevel;
    private TextView rssiLevel;
    private TextView chargingLevel;

    private Switch switch1;
    private Switch switch2;
    private Switch switch3;
    private Switch switch4;
    private Switch switch5;
    private Switch switch6;

    private Button button_start;
    private Button button_stop;

    private BluetoothGattCharacteristic batteryLevelCharacteristic;

    private BluetoothGattCharacteristic sphereTestCharacteristic;
    private BluetoothGattCharacteristic powerRailsCharacteristic;
    private BluetoothGattCharacteristic sphereStatusCharacteristic;

    private boolean writing = false;
    private boolean disAvailable = false;
    private boolean switch1changed = false;
    private boolean switch2changed = false;
    private boolean switch3changed = false;
    private boolean switch4changed = false;
    private boolean switch5changed = false;
    private boolean switch6changed = false;


    private boolean ble_ready = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_view);
        this.readQueue = new ConcurrentLinkedQueue<>();

        address = (TextView) findViewById(R.id.address);
        batteryLevel = (TextView) findViewById(R.id.battery_level);
        humidityLevel = (TextView) findViewById(R.id.humidity_level);
        temperatureLevel = (TextView) findViewById(R.id.temperature_level);
        pressureLevel = (TextView) findViewById(R.id.pressure_level);
        statusLevel = (TextView) findViewById(R.id.status_level);
        rssiLevel = (TextView) findViewById(R.id.RSSI);
        chargingLevel = (TextView) findViewById(R.id.charging_level);

        button_start = (Button) findViewById(R.id.button);
        button_stop = (Button) findViewById(R.id.button2);


        switch1 = (Switch) findViewById(R.id.switch1);
        switch2 = (Switch) findViewById(R.id.switch2);
        switch3 = (Switch) findViewById(R.id.switch3);
        switch4 = (Switch) findViewById(R.id.switch4);
        switch5 = (Switch) findViewById(R.id.switch5);
        switch6 = (Switch) findViewById(R.id.switch6);


        switch1.setChecked(false);
        switch2.setChecked(false);
        switch3.setChecked(false);
        switch4.setChecked(false);
        switch5.setChecked(false);
        switch6.setChecked(false);


        button_start.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Log.i("button Start", " click");
                switch1.setChecked(true);
                switch2.setChecked(true);
                switch3.setChecked(true);
                switch4.setChecked(true);
                switch5.setChecked(true);
            }
        });

        button_stop.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Log.i("button Stop", " click");
                switch1.setChecked(false);
                switch2.setChecked(false);
                switch3.setChecked(false);
                switch4.setChecked(false);
                switch5.setChecked(false);
                switch6.setChecked(false);

            }
        });

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                switch1changed = true;
            }
        });

        switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                switch2changed = true;
            }
        });

        switch3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                switch3changed = true;
            }
        });

        switch4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                switch4changed = true;
            }
        });

        switch5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                switch5changed = true;
            }
        });

        switch6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                switch6changed = true;
            }
        });

        Intent intent = getIntent();
        device = (BluetoothDevice) intent.getExtras().get("device");
        address.setText(device.getAddress());

        connectToDevice(device);


        final ExecutorService es = Executors.newCachedThreadPool();
        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                es.submit(new Runnable() {
                    @Override
                    public void run() {
                        if (switch1changed || switch2changed || switch3changed || switch4changed || switch5changed || switch6changed) {
                            sendPowerCmd();
                            switch1changed = false;
                            switch2changed = false;
                            switch3changed = false;
                            switch4changed = false;
                            switch5changed = false;
                            switch6changed = false;

                        }
                        readQueue.offer(sphereStatusCharacteristic);
                        readQueue();
                    }
                });
            }
        }, 0, 1, TimeUnit.SECONDS);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }



    public void connectToDevice(BluetoothDevice device) {
        Log.i("connectToDevice", " connect to device");
        if (mGatt == null) {
            mGatt = device.connectGatt(this, false, gattCallback);
            mGatt.discoverServices();

        } else {
            mGatt.discoverServices();
        }
    }

    public boolean sendPowerCmd() {
        powerRailsCharacteristic.setValue(procesState());
        return mGatt.writeCharacteristic(powerRailsCharacteristic);
    }

    public void readQueue() {
        if (mGatt != null) {
            BluetoothGattCharacteristic nextRequest = readQueue.poll();
            if (nextRequest != null) {
                // Send a read request for the next item in the queue
                if (!writing) mGatt.readCharacteristic(nextRequest);
            } else {
                // We've reached the end of the queue
                disAvailable = true;
                boolean rssiStatus = mGatt.readRemoteRssi();

            }
        }

    }


    public final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i("gattCallback", "STATE_CONNECTED");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.e("gattCallback", "STATE_DISCONNECTED");
                    finish();
                    break;
                default:
                    Log.e("gattCallback", "STATE_OTHER");
            }

        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Log.d("onReadRemoteRssi", "Received RSSI");
            writeLine(String.valueOf(rssi),rssiLevel);
        }


        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            // batteryLevelCharacteristic = gatt.getService(Battery_Service_UUID).getCharacteristic(Battery_Level_UUID);


            // Discover characteristics associated with Serice
            powerRailsCharacteristic = gatt.getService(SphereInfo_Service_UUID).getCharacteristic(PowerRails_Characteristic_UUID);
            sphereStatusCharacteristic = gatt.getService(SphereInfo_Service_UUID).getCharacteristic(SphereStatus_Characteristic_UUID);
            sphereTestCharacteristic = gatt.getService(SphereInfo_Service_UUID).getCharacteristic(SphereTest_Characteristic_UUID);


            // Set up notification updates
            mGatt.setCharacteristicNotification(sphereTestCharacteristic, true);
            UUID uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
            BluetoothGattDescriptor descriptor = sphereTestCharacteristic.getDescriptor(uuid);

            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
            boolean success = gatt.writeDescriptor(descriptor);
            Log.d("onServiceDiscovered", "Got " + success);
            ble_ready = true;

            readQueue.offer(powerRailsCharacteristic);
            readQueue.offer(sphereStatusCharacteristic);


            //gatt.readCharacteristic(sphereStatusCharacteristic);
            //gatt.readCharacteristic(powerRailsCharacteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.d("onCharacteriscChanged", "");
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (characteristic.getUuid().equals(Battery_Level_UUID)) {
                    writeLine(longToString(characteristic.getValue()) + "%", batteryLevel);
                } else if (characteristic.getUuid().equals(PowerRails_Characteristic_UUID)) {
                    byte[] array = characteristic.getValue();

                    if (array[0] == 0x01) {
                        updateSwitch(switch1, true);
                        Log.d("onCharacteristicRead", "Set switch");
                    }
                    if (array[1] == 0x01) updateSwitch(switch2, true);
                    if (array[2] == 0x01) updateSwitch(switch3, true);
                    if (array[3] == 0x01) updateSwitch(switch4, true);
                    if (array[4] == 0x01) updateSwitch(switch5, true);
                    if (array[5] == 0x01) updateSwitch(switch6, true);

                    Log.d("onCharacteristicRead", "Read Power rails status " + encodeHexString(array));
                    readQueue();

                } else if (characteristic.getUuid().equals(SphereStatus_Characteristic_UUID)) {
                    byte[] array = characteristic.getValue();
                    writeLine(getFloatFromArray(Arrays.copyOfRange(array, 0, 4)) + " ℃", temperatureLevel);
                    writeLine(getFloatFromArray(Arrays.copyOfRange(array, 4, 8)) + " kPa", pressureLevel);
                    writeLine(getFloatFromArray(Arrays.copyOfRange(array, 8, 12)) + " %", humidityLevel);
                    writeLine(getFloatFromArray(Arrays.copyOfRange(array, 12, 16)) + "", statusLevel);
                    writeLine(getFloatFromArray(Arrays.copyOfRange(array, 16, 20)) + "A", chargingLevel);
                    Log.d("onCharacteristicRead", "Read Sphere Status");
                    readQueue();
                }

            }
        }

        @Override
        public final void onDescriptorWrite(final BluetoothGatt gatt, final BluetoothGattDescriptor descriptor, final int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.i("Data written to descr. " + descriptor.getUuid(), ", value: " + descriptor);
            } else {
                Log.i("Error", " value: ");

            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            writing = false;
        }


    };

    private long toNumber(byte[] byteArray) {
        long value = 0;
        for (int i = 0; i < byteArray.length; i++) {
            value += ((long) byteArray[i] & 0xffL) << (8 * i);
        }
        return value;
    }

    private String addComma(String value, int numberOfZeros) {
        return value.substring(0, value.length() - numberOfZeros) + "," + value.substring(value.length() - numberOfZeros, value.length());
    }

    private String longToString(byte[] byteArray) {
        return Long.toString(toNumber(byteArray));
    }

    private String addCommaFromByteArray(byte[] array, int numberOfZeros) {
        return addComma(longToString(array), numberOfZeros);
    }

    private void writeLine(final String text, final TextView textView) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setText(text);
            }
        });
    }

    private void updateSwitch(final Switch switch_var, final boolean switch_state) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch_var.setChecked(switch_state);
            }
        });
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private byte[] procesState() {
        byte[] array = new byte[6];
        if (switch1.isChecked()) {
            array[0] = Integer.decode("0x01").byteValue();
        } else {
            array[0] = Integer.decode("0x00").byteValue();
        }
        if (switch2.isChecked()) {
            array[1] = Integer.decode("0x01").byteValue();
        } else {
            array[1] = Integer.decode("0x00").byteValue();
        }
        if (switch3.isChecked()) {
            array[2] = Integer.decode("0x01").byteValue();
        } else {
            array[2] = Integer.decode("0x00").byteValue();
        }
        if (switch4.isChecked()) {
            array[3] = Integer.decode("0x01").byteValue();
        } else {
            array[3] = Integer.decode("0x00").byteValue();
        }
        if (switch5.isChecked()) {
            array[4] = Integer.decode("0x01").byteValue();
        } else {
            array[4] = Integer.decode("0x00").byteValue();
        }
        if (switch6.isChecked()) {
            array[5] = Integer.decode("0x01").byteValue();
        } else {
            array[5] = Integer.decode("0x00").byteValue();
        }
        return array;
    }


    public static byte[] toByteArray(double value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    public double toDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }

    public float toFloat(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getFloat();
    }


    public String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }

    public static void reverse(byte[] array) {
        if (array == null) {
            return;
        }
        int i = 0;
        int j = array.length - 1;
        byte tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }

    public float getFloatFromArray(byte[] array) {
        reverse(array);
        float data = toFloat(array);
        return data;
    }

    public String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Device Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("onPause:", " Paused");
    }

    @Override
    public void onStop() {

        super.onStop();
        mGatt.close();
        Log.d("onStop:", "Disconnected from device");
        finish();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }
}
